import requests
import os
from dotenv import load_dotenv
import telebot
from telebot.types import ReplyKeyboardMarkup, KeyboardButton

load_dotenv()

key_for_bot = os.getenv('API_KEY_FROM_BOT')


bot = telebot.TeleBot(key_for_bot)

markup = ReplyKeyboardMarkup(resize_keyboard=True)
button1 = KeyboardButton("Фото")
button2 = KeyboardButton("Видео")
markup.row(button1, button2)


@bot.message_handler(commands=['start'])
def start(message):
    bot.send_message(chat_id=message.chat.id, text="Собачки ждут!!", reply_markup=markup)


@bot.message_handler(func=lambda message: message.text in ['Фото', 'Видео'])
def handle_message(message):
    if message.text == "Фото":
        doggy_response = requests.get('https://random.dog/woof.json').json().get('url')
        bot.send_photo(chat_id=message.chat.id, photo=doggy_response)
    elif message.text == "Видео":
        doggy_response = requests.get('https://random.dog/woof.json').json().get('url')
        while not (doggy_response.endswith('.mp4') or doggy_response.endswith('.gif') or doggy_response.endswith('.webm')):
            doggy_response = requests.get('https://random.dog/woof.json').json().get('url')
        bot.send_video(chat_id=message.chat.id, video=doggy_response)


bot.polling(none_stop=True)
